//
//  CalculatorViewController.swift
//  Calculatrice
//
//  Created by Yacine Salhi on 13/01/2017.
//  Copyright © 2017 Yellow Submarine. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {
    
    @IBOutlet weak var display: UILabel!
    
    let calculator = Calculator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: User Action
extension CalculatorViewController {
    
    // Handle only one digit
    @IBAction func touchNumber(_ sender: UIButton) {
        if calculator.isCalculating() {
            calculator.result(otherNumber: sender.tag)
        } else {
            calculator.add(number: sender.tag)
        }
        display.text = String(sender.tag)
    }
    
    @IBAction func addition(_ sender: UIButton) {
        calculator.add(operation: .addition)
        display.text = "+"
    }
    
    @IBAction func result(_ sender: Any) {
        display.text = calculator.actualDisplay()
    }
    
    @IBAction func clear(_ sender: Any) {
        calculator.clear()
        display.text = ""
        print("Clear Result")
    }
}

// MARK: Operation handled
enum Operator {
    case addition
    case substraction
}

// MARK: Calculator Model
class Calculator {
    var firstNumber: Int = 0
    var operation: Operator?
    
    func add(number: Int) {
        firstNumber = number
    }
    
    func add(operation: Operator) {
        self.operation = operation
    }
    
    func isCalculating() -> Bool {
        return (operation != nil)
    }
    
    func clear() {
        self.operation = nil
        self.firstNumber = 0
    }
    
    func result(otherNumber: Int) {
        if let opp = operation {
            switch opp {
            case .addition:
                firstNumber = addition(first: firstNumber, second: otherNumber)
            case .substraction:
                firstNumber = substraction(first: firstNumber, second: otherNumber)
            }
        }
    }
    
    func actualDisplay() -> String {
        return "\(firstNumber)"
    }
    
    // MARK: != Operation
    private func addition(first: Int, second: Int) -> Int {
        return first + second
    }
    
    private func substraction(first: Int, second: Int) -> Int {
        return first + second
    }
}
